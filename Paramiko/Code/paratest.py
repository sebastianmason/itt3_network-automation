import paramiko
import time
import os
import traceback

ip          = '192.168.10.1'
user        = 'python'
password    = 'qaz123'



try:
    sshClient = paramiko.SSHClient()                    # Create SSHClient object
    sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print ('Trying to connect to 192.168.10.1')
    sshClient.connect(ip, username=user, key_filename='rsa')

    with sshClient.invoke_shell() as channel:           # Create a channel object
        print ('SRX shell invoked')
        os.system('cls')                                # Clear/Whipe the terminal window
        channel.send('cli\n')                           # Enter router the Command Line Interface
        time.sleep(1)                                   # Leave time for the router to enter CLI
        routerOutput = channel.recv(1000)               # Read router. Output/replies not used
        # time.sleep(3)                                 # Wait to buffer times out and is empty
        channel.send('show configuration | no-more\n')  # Make router list config
        time.sleep(0.7)                                 # Leave time for the router to list config
        output = channel.recv(10000)                    # Read router configuration output
        output = output.decode().replace("\r","")       # Do some text formatting
        print (output)                                  # List the configuration in the terminal window
        channel.close()                                 # Close channel and its underlying Transport

    sshClient.close()                                   # Close SSHClient and its underlying Transport

    with open('SRXconf.cfg', 'w') as outFile:
        outFile.write(output)                           # Save the configuration in a file

except Exception:
    print ('Something went wrong:')
    traceback.print_exc()
