#!/usr/bin/env python
import threading
import os
import gzip
import shutil
import argparse
from datetime import datetime
from netmiko import SSHDetect, SCPConn, Netmiko

cfg_file = "srx2.conf"
key_file = "rsa"

commands = ["set system host-name vSRX_test", "commit"]



junos1_ssh = {
    "host": "192.168.10.1",
    "username": "python",
    "device_type": "juniper_junos",
    "use_keys": True,
    "key_file": key_file,
    "ssh_config_file": "./ssh_config"
}

junos2_pass = {
    "host": "192.168.10.3",
    "username": "root",
    "password": "qaz123",
    "device_type": "juniper_junos",
}

junos3_ssh_detect = {
    "host": "192.168.10.1",
    "username": "python",
    "device_type": "autodetect",
    "use_keys": True,
    "key_file": key_file,
    "ssh_config_file": "./ssh_config"
}

def get_args():
    parser = argparse.ArgumentParser(description='Do some basic Netmiko operations')
    parser.add_argument('-c', '--choice',  dest="choice", help='choose what to do')

    return parser.parse_args()

def guess_connect(device):
    guesser = SSHDetect(**device)
    best_match = guesser.autodetect()
    print(best_match)  # Name of the best device_type to use further
    print(guesser.potential_matches)  # Dictionary of the whole matching result
    device["device_type"] = best_match
    with Netmiko(**device) as connection:
        print(connection.find_prompt())
    
def list_connect(devices):
    for device in devices:
        with Netmiko(**device) as net_connect:
            print(net_connect.find_prompt())                         # Show cli prompt
            #output = net_connect.send_config_set(commands)          # Send multiple commands
            output = net_connect.send_config_from_file(cfg_file)    # Send config from file
    #        output = net_connect.send_command("show route terse")            # Send single command
            print(output)

def show_version(device):
    """Execute show version command using Netmiko."""
    with Netmiko(**device) as remote_conn:
        print()
        print("#" * 80)
        print(remote_conn.send_command_expect("show version"))
        print("#" * 80)
        print()

def threded_connect(devices):
    """
    Use threads and Netmiko to connect to each of the devices. Execute
    'show version' on each device. Record the amount of time required to do this.
    """
    start_time = datetime.now()

    for device in devices:
        my_thread = threading.Thread(target=show_version, args=(device,))
        my_thread.start()

    main_thread = threading.currentThread()
    for some_thread in threading.enumerate():
        if some_thread != main_thread:
            print(some_thread)
            some_thread.join()

    print("\nElapsed time: " + str(datetime.now() - start_time))

def send_config_from_file(devices, config):
    if type(devices) is not list:
        devices = [devices]
    for device in devices:
        with Netmiko(**device) as ssh_conn:
            output = ssh_conn.send_config_from_file(config)    # Send config from file
            output += ssh_conn.commit()
            print (output)

def SCP_get_config(devices):
    if type(devices) is not list:
        devices = [devices]
    for device in devices:
        filename = f'{device["host"]}_vSRX.conf'
        with Netmiko(**device) as ssh_conn:
            scp = SCPConn(ssh_conn)
            scp.scp_get_file("/config/juniper.conf.gz", f"{filename}.gz")
            scp.close()
        
        with gzip.open(f"{filename}.gz", "rb") as zip_file:
            with open(filename,"wb") as config_file:
                shutil.copyfileobj(zip_file, config_file)
        
        if os.path.exists(f"{filename}.gz"):
            os.remove(f"{filename}.gz")


choices = {
    "get": SCP_get_config,
    "send_file": send_config_from_file,
    "send_command": list_connect
}


if __name__ == "__main__":
    devices = [junos1_ssh, junos2_pass]
    params = get_args()
    if params.choice in choices.keys():
        choices[params.choice](devices)
    else:
        print(f"{params.choice} is not a valid choice")
    #guess_connect(junos3)
    #list_connect(devices)
    #threded_connect(devices)
    #SCP_get_config(devices)
    #send_config_from_file(junos2_pass, cfg_file)
