from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from lxml import etree
from sys import argv


#Saves config to a XML-file
def saveConfigToFile(config):
    with open("config.xml","w") as handler:
        handler.write(config)


def changeValue(root, name, value):
    for elem in root:
        name=elem.find("host-name")
        if name is not None:
            print("Current host-name:\t", name.text)
            #Replace host-name
            name.text=value


def main(hostname="cookiebox"):
    with Device(host='192.168.10.1', user='root', password='qaz123' ) as dev:
            
        #Get config from router and save to file
        config=etree.tostring(dev.rpc.get_config()).decode()
        saveConfigToFile(config)

        #Load config into XML element
        tree=etree.parse("config.xml")
        root=tree.getroot()

        #Find host-name entry in config
        changeValue(root, "host-name", hostname)

        #Save changed config to file
        config = etree.tostring(tree).decode()
        saveConfigToFile(config)
        
        with Config(dev) as cu:
            
            #Lock the candidate config
            cu.lock()

            #Upload config to juniper device candidate config
            cu.load(etree.tostring(tree).decode(), merge=True)

            #Commit changes
            if cu.commit_check():
                cu.commit()
                print("New host-name:\t\t", hostname)

            #Unlock the configuration
            cu.unlock()


if __name__ == "__main__":
    if len(argv) > 1:
        main(argv[1])
    else:
        main()