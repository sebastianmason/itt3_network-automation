from jnpr.junos import Device
from pprint import pprint
import sys
from lxml import etree
import xmltodict


hostIP = '192.168.10.1'

try:
	# default device to connect, added timeout for device
	Device.auto_probe = 3
	dev = Device(host = hostIP, user="root", password="qaz123")
	print ('Opening connection to ',hostIP)
	dev.open()
	#print(dev.facts)
except Exception as somethingIsWrong:
	print ("Unable to connect to host:", somethingIsWrong)
	sys.exit(1)

print(etree.tostring(dev.rpc.get_route_information({'format':'text'}, terse=True)).decode())
#print (etree.tostring(dev.rpc.get_route_information({'format':'text'},terse=True)).decode().replace("&gt;",">").replace("<output>","").replace("</output>",""))

dev.close()





