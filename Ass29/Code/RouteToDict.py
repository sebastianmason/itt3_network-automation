from jnpr.junos import Device
from pprint import pprint
import sys
from lxml import etree
import xmltodict
from queue import Queue


hostIP = '192.168.10.1'

try:
	# default device to connect, added timeout for device
	Device.auto_probe = 3
	dev = Device(host = hostIP, user="root", password="qaz123")
	print ('Opening connection to ',hostIP)
	dev.open()
	#print(dev.facts)
except Exception as somethingIsWrong:
	print ("Unable to connect to host:", somethingIsWrong)
	sys.exit(1)

# Convert output to dictionary
xml = etree.tostring(dev.rpc.get_route_information(terse=True)).decode()
xmlDict = xmltodict.parse(xml)

# Print routes from dictionary
for dest in xmlDict["route-information"]["route-table"]["rt"]:
	route = dest["rt-destination"] + "       \t--->\t"
	if "nh" in dest["rt-entry"].keys():
		if "to" in dest["rt-entry"]["nh"].keys():
			route += dest["rt-entry"]["nh"]["to"]
		else:
			route += dest["rt-entry"]["nh"]["via"]
	else:
		route += dest["rt-entry"]["nh-type"]
	
	print(route)

# End connection to router
dev.close()





